/*************************************************
The MIT License (MIT)
	
Copyright (c) 2014 Mikael "DevMikaelNilsson" Nilsson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ObjectPainter
{
	/// <summary>
	/// PaintManager script stores all PaintObject instances which are added to its internal list in a scene.
	/// The PaintManager is normally only accessed and used through the ObjectPainterEditor window, since the objects in the list
	/// doesn't need to be accessed during gameplay.
	/// </summary>
	[System.Serializable]
	public class PaintManager
	{
		/// <summary>
		/// Type of objects supported by the PaintManager.
		/// </summary>
		[HideInInspector]
		public enum CurrentPaintObjectType
		{
			/// <summary>
			/// Normal GameObjects, including Sprite objects.
			/// </summary>
			GameObject = 0
		}

		[SerializeField]
		private CurrentPaintObjectType m_displayCurrentObjectType = CurrentPaintObjectType.GameObject;
		
		[HideInInspector]
		[SerializeField]
		private List<PaintObject> m_objectPainterGameObjectList = new List<PaintObject>();

		[SerializeField]
		private PaintObject m_currentActivePaintObject = null;

		/// <summary>
		/// Get/Set the current type of objects for the PaitnManager to display and work with.
		/// </summary>
		[HideInInspector]
		public CurrentPaintObjectType DisplayCurrentObjectType
		{
			get
			{
				return m_displayCurrentObjectType;
			}
			set
			{
				m_displayCurrentObjectType = value;
			}
		}

		/// <summary>
		/// Get the number of stored objects in the internal list. Return -1 if the list is not valid or missing.
		/// </summary>
		[HideInInspector]
		public int ObjectCount 
		{ 
			get 
			{ 
				switch(m_displayCurrentObjectType)
				{
					case CurrentPaintObjectType.GameObject:
						return m_objectPainterGameObjectList.Count;
					default:
						return -1;
				}
			} 
		}

		/// <summary>
		/// Retrieves the proper list based on what type of Object is currently being used.
		/// All different objects should have its own specific object list. 
		/// </summary>
		/// <returns>The current active list based on what object type is currently active. Returns null if the current object type is not supported.</returns>
		private List<PaintObject> GetCurrentActiveList()
		{
			switch (m_displayCurrentObjectType)
			{
			case CurrentPaintObjectType.GameObject:
				return m_objectPainterGameObjectList;
			default:
				StringBuilder errorString = new StringBuilder();
				errorString.Append("The following object type if not supported: ");
				errorString.Append(m_displayCurrentObjectType.ToString());
				Debug.LogError(errorString.ToString());
				return null;
			}
		}

		/// <summary>
		/// Retrieve a PaintObject instance from the internal list, based on its index value.
		/// </summary>
		/// <param name="index">The index value for the PaintObject. The value is zero (0) based.</param>
		/// <returns>The found PaintObject based on the index value. Return null if there was no PaintObject found with that index.</returns>
		public PaintObject GetPainterObject(int index)
		{
			List<PaintObject> currentObjectList = GetCurrentActiveList();

			if (index < 0 || index > currentObjectList.Count)
				return null;
			else
				return currentObjectList[index];
		}

		/// <summary>
		/// Retrieves a PaintObject based on the shortcut index for the PaintObject.
		/// A shortcut index is used to get what shortcut key the PaintObject has. 
		/// The list of shortcut keys can be found in the ObjectPainterWindow class.
		/// </summary>
		/// <param name="shortcutIndex">The index value for the shortcut list. The value is zero (0) based.</param>
		/// <returns><The PaintObject with the matching shortcutIndex value. Return null if a object was not found.</returns>
		public PaintObject GetShortcutPainterObject(int shortcutIndex)
		{
			List<PaintObject> currentObjectList = GetCurrentActiveList();

			if (shortcutIndex < 0 || shortcutIndex > 9)
				return null;
			else
			{
				int objectCount = currentObjectList.Count;
				for (int i = 0; i < objectCount; ++i)
				{
					int testValue = (shortcutIndex);
					if (currentObjectList[i].ShortCutIndex == testValue)
						return currentObjectList[i];
				}
			}

			return null;
		}

		/// <summary>
		/// Retrieves a PaintObject index value based on the shortcut index for the PaintObject.
		/// A shortcut index is used to get what shortcut key the PaintObject has. 
		/// The list of shortcut keys can be found in the ObjectPainterWindow class.
		/// </summary>
		/// <param name="shortcutIndex">The index value for the shortcut list. The value is zero (0) based.</param>
		/// <returns><The PaintObject index value with the matching shortcutIndex value. Return -1 if a object was not found.</returns>
		public int GetShortcutObjectIndex(int shortcutIndex)
		{
			List<PaintObject> currentObjectList = GetCurrentActiveList();

			if (shortcutIndex < 0 || shortcutIndex > 9)
				return -1;
			else
			{
				int objectCount = currentObjectList.Count;
				for (int i = 0; i < objectCount; ++i)
				{
					int testValue = (shortcutIndex);
					if (currentObjectList[i].ShortCutIndex == testValue)
						return i;
				}
			}

			return -1;
		}

		/// <summary>
		/// Creates a new PaintObject and adds it to the internal list.
		/// </summary>
		/// <param name="newGameObject">A valid GameObject which will be linked to this PaintObject object. This value can not be null.</param>
		/// <param name="allowDuplicate"></param>
		public void CreatePainterObject(GameObject newGameObject, bool allowDuplicate)
		{
			if(newGameObject == null)
				return;

			if(allowDuplicate == false)
			{
				List<PaintObject> currentObjectList = GetCurrentActiveList();
				int objectCount = currentObjectList.Count;
				for(int i = 0; i < objectCount; ++i)
				{
					if(currentObjectList[i].PaintGameObject == newGameObject)
					{
						StringBuilder warningText = new StringBuilder();
						warningText.Append("Object (");
						warningText.Append(newGameObject.name);
						warningText.Append(") already exists in list.");
						Debug.LogWarning( warningText.ToString());
						return;
					}
				}
			}

			PaintObject newPaintObject = new PaintObject();
			newPaintObject.PaintGameObject = newGameObject;
			AddPainterObject(newPaintObject);
		}

		/// <summary>
		/// Adds a gameobject to a existing PaintObject. This method will overwrite the previous existing GameObject.
		/// </summary>
		/// <param name="currentPaintObject">The PaintObject object to add the GameObject to. This value can not be null.</param>
		/// <param name="newGameObject">The new GameObject to link to the PaintObject. This value can not be null.</param>
		/// <param name="allowDuplicate">Set to true if there can be several PaintObjects with the same GameObject linked to it. Set to false otherwise.</param>
		public void AddGameObjectToList(PaintObject currentPaintObject, GameObject newGameObject, bool allowDuplicate)
		{
			if (currentPaintObject ==  null || newGameObject == null)
				return;

			if(allowDuplicate == false)
			{
				List<PaintObject> currentObjectList = GetCurrentActiveList();
				int objectCount = currentObjectList.Count;
				for (int i = 0; i < objectCount; ++i)
				{
					if (currentObjectList[i].PaintGameObject == newGameObject)
					{
						StringBuilder warningText = new StringBuilder();
						warningText.Append("Object (");
						warningText.Append(newGameObject.name);
						warningText.Append(") already exists in list.");
						Debug.LogWarning(warningText.ToString());
						return; 
					}
				}
			}

			currentPaintObject.PaintGameObject = newGameObject;
		}

		/// <summary>
		/// Add a PaintObject to the internal list.
		/// </summary>
		/// <param name="painterObject">The new PaintObject to add to the internal list.</param>
		public void AddPainterObject(PaintObject painterObject)
		{
			List<PaintObject> currentObjectList = GetCurrentActiveList();
			currentObjectList.Add(painterObject);
		}

		/// <summary>
		/// Removes a PaintObject based on its index value from the internal list.
		/// </summary>
		/// <param name="index">The index value for the PaintObject to be removed.</param>
		/// <returns>Returns true if the PaintObject was successfully removed. Returns false otherwise.</returns>
		public bool RemovePainterObject(int index)
		{
			List<PaintObject> currentObjectList = GetCurrentActiveList();
			if (index < 0 || index > currentObjectList.Count)
				return false;
			else
				currentObjectList.RemoveAt(index);

			return true;
		}

		/// <summary>
		/// Retrieves the current active PaintObject. There can only be one active PaintObject per list.
		/// The active PaintObject has normally a tick in its active box in the object list, found in the ObjectPainterEditor window.
		/// </summary>
		/// <returns>The current active PaintObject. Returns null if there is no active PaintObject.</returns>
		public PaintObject GetActivePainterObject()
		{
			return m_currentActivePaintObject;
		}

		/// <summary>
		/// Set a PaintObject to be the active PaintObject based on its idex value.
		/// </summary>
		/// <param name="index">The index value for the PaintObject which will be set as the active object. This value is zero (0) based.</param>
		public void SetActivePainterObject(int index)
		{
			List<PaintObject> currentObjectList = GetCurrentActiveList();
			m_currentActivePaintObject = null;
			int objectCount = currentObjectList.Count;
			for (int i = 0; i < objectCount; ++i)
			{
				if (i == index)
				{
					currentObjectList[i].IsActive = true;
					m_currentActivePaintObject = currentObjectList[i];
				}
				else
					currentObjectList[i].IsActive = false;
			}
		}

		/// <summary>
		/// Set a shortcut index value for a PaintObject based on its index value.
		/// </summary>
		/// <param name="index">The index value for the PaintObject which will recieve the shortcut index.</param>
		/// <param name="newShortcutIndex">The shortcut index value which will be linked to the PaintObject.</param>
		public void SetShortcutIndex(int index, int newShortcutIndex)
		{
			List<PaintObject> currentObjectList = GetCurrentActiveList();

			if(newShortcutIndex != 0)
			{
				if (newShortcutIndex < 0 || newShortcutIndex > 9)
					return;
				else if (index < 0 || index > currentObjectList.Count)
					return;

				int objectCount = currentObjectList.Count;
				for (int i = 0; i < objectCount; ++i)
				{
					if (index == i)
						continue;

					if (currentObjectList[i].ShortCutIndex == newShortcutIndex)
						return;
				}
			}

			currentObjectList[index].ShortCutIndex = newShortcutIndex;
		}
	}
}
