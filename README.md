# Object Painter v 1.0 #

### Summary ###
This is a simple, but powerfull, object painter/level editor for the Unity 3d application.
The Object Painter is a editor plugin which allows you to add GameObjects/prefabs into a list, and then by activating a object, to put the object in the level by clicking in the Scene view window.

Object Painter can be downloaded as a Unitypackage in the download section.

### Features ###
* Released under the MIT license.
* Paint (position) objects into the scene view without any need to drag and drop objects.
* 3D and 2D (sprites) objects supported.
* Add or remove paintable objects directly in the Object Painter Window
* Drag and drop a single or multiple objects/prefabs direct into the object list.
* Switch between different objects by either using the Object Painter Editor window, or simply use one of the shortcut keys.
* Up to 12 different shortcut keys are supported.
* It's easy to change the controls on how to use the Object Painter.
* Snap a 3D or a 2D (Sprite) object onto another object without the need to reposition the object afterwards.
* The Object Painter runs in the background, no need to have the Window visible in order to use the Object Painter functionality.

### How to contribute to project ###

When working with this project, the standard microsoft C# programming Guidelines must be respected and used.
The project is basically using the CamelCase and mixedCase when writing code.

* Types *(class, struct, delegate, enum)* - **CamelCase**
* Properties *(All Access Level, static und non-static)* - **CamelCase**
* Local variables and parameters - **mixedCase**
* Member variables *(non-static, public)* - **CamelCase**
* Member variables *(non-static, protected, private,.)* - **m_mixedCase**
* Member variables *(static, const bzw. readonly)* - **CamelCase**
* Member variables, *(static, public)* - **CamelCase**
* Member variables *(static, protected, private,..)* - **s_mixedCase**

Example code:
```
#!c#
using System.Diagnostics;
using System.Threading;
 
namespace ObjectPainter
{
    public class Coyote
    {
        public const double MaxSpeed = 250;
        public double Speed { get; set; }
         
        public bool IsRunning
        {
            get { return m_isRunning; }
        }
         
        public Coyote()
        {
            Interlocked.Increment(ref s_instanceCount);
        }
         
        public void Run()
        {
            m_isRunning = true;
            double totalDistance = 0.0;
            for (int i = 0; i < 1000; i++)
            {
                totalDistance += (Speed / 1000.0);
                Trace.TraceInformation("Total distance: {0}", totalDistance);
            }
        }
         
        private static int s_instanceCount;
        private bool m_isRunning;
    }
}

```


### Screenshots ###

Editor window

![PainterObjectEditorWindow.png](https://bitbucket.org/repo/ronjbq/images/1044396486-PainterObjectEditorWindow.png)

Full window (Unity3D v.4.3.4f1 Pro version)

![PainterObjectFullWindow.png](https://bitbucket.org/repo/ronjbq/images/1661777282-PainterObjectFullWindow.png)