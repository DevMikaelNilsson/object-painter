/*************************************************
The MIT License (MIT)
	
Copyright (c) 2014 Mikael "DevMikaelNilsson" Nilsson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*************************************************/
using UnityEngine;
using UnityEditor;
using System.Collections;
using ObjectPainter;

/// <summary>
/// The main editor window for the ObjectPainter plugin.
/// This script displays all possibilites which the user has to build a level.
/// The script displays the GUI objects for the editor, and does not really handle any inputs
/// more than directly from the GUI objects themselves.
/// </summary>
[InitializeOnLoad]
public class ObjectPainterWindow : EditorWindow
{
	/// <summary>
	/// Supported shortcut keys which will be displayed as a list for the user.
	/// These values are for display only and is not stored anywhere. The index value
	/// for thse values are linked with the PaintObject which will use the shortcut.
	/// </summary>
	private string[] m_shortcutValue = new string[]
	{
		"-",
		"F1",
		"F2",
		"F3",
		"F4",
		"F5",
		"F6",
		"F7",
		"F8",
		"F9",
		"F10",
		"F11",
		"F12"
	};

	/// <summary>
	/// Property settings for when a user places a object which does not have a target object 
	/// (the user does not click on a object when activating the paint method).
	/// </summary>
	private string[] m_emptyObjectPlacementProperty = new string[]
	{
		"on a given distance to camera",
		"on a given world space position"	
	};

	/// <summary>
	/// Supported keyboard input shortcuts.
	/// </summary>
	protected enum KeyboardShortcutKeys
	{
		None = 0,
		Ctrl = 1,
		Shift = 2
	}

	/// <summary>
	/// Supported mouse input shortcuts.
	/// </summary>
	protected enum MouseInputType
	{
		/// <summary>
		/// Left mouse button.
		/// </summary>
		LMB = 0,

		/// <summary>
		/// Right mouse button.
		/// </summary>
		RMB = 1
	}

	protected static MouseInputType s_mouseSnapToObject = MouseInputType.LMB;
	protected static MouseInputType s_mouseAddObjectToScene = MouseInputType.LMB;
	protected static KeyboardShortcutKeys s_keyboardSnapToObject = KeyboardShortcutKeys.Shift;
	protected static KeyboardShortcutKeys s_keyboardAddObjectToScene = KeyboardShortcutKeys.Ctrl;
	protected static int s_emptyObjectPlacementPropertyIndex = 0;
	protected static Vector3 s_emptyObjectPlacementPropertyVector = new Vector3(20.0f, 20.0f, 20.0f);
	protected static PaintManager s_objectPainterInstance = null;

	private static Vector2 s_windowSize = new Vector2(580.0f, 300.0f);
	private Rect m_objectPropertiesWindow = new Rect(10.0f, 10.0f, 300.0f, 250.0f);
	private Vector2 m_mainWindowScrollPos = Vector2.zero;
	private Vector2 m_objectListScrollPos = Vector2.zero;
	
	/// <summary>
	/// Initalizes the window when it gets created. This is normally called when the window is opened for the first time.
	/// </summary>
	[MenuItem("Assets/ObjectPainter")]
	public static void init()
	{
		//Show existing window instance. If one doesn't exist, make one.
		EditorWindow editorWindow = EditorWindow.GetWindow(typeof(ObjectPainterEditor));
		editorWindow.autoRepaintOnSceneChange = true;
		editorWindow.Show();
	}

	/// <summary>
	/// Constructor where I add my delegate methods.
	/// </summary>
	public ObjectPainterWindow()
	{
		SceneView.onSceneGUIDelegate += ObjectPainterEditor.UpdateView;
		EditorApplication.update += Update;
	}

	/// <summary>
	/// Repaints the window every update, so that its repainted eventhough the window is not in focus.
	/// </summary>
	void Update()
	{
		Repaint();
	}

	/// <summary>
	/// GUI render method.
	/// This method is called several times per frame, depeding on the internal Unity editor settings.
	/// The method renders all our GUI objects and their properties,
	/// </summary>
	void OnGUI()
	{
		if (s_objectPainterInstance == null)
			s_objectPainterInstance = new PaintManager();

		m_mainWindowScrollPos = GUI.BeginScrollView(new Rect(0, 0, position.width, position.height), m_mainWindowScrollPos, new Rect(0.0f, 0.0f, s_windowSize.x, s_windowSize.y));

		DisplayPaintObjectProperties();
		DisplayKeyboardProperties();
		CheckForDroppedObjects();
		GUI.EndScrollView();
	}

	/// <summary>
	/// This method checks if there is any objects dragged and dropped into the same area as the object list.
	/// If there is one, or several objects, dropped into this area, they are added to the list. 
	/// </summary>
	private void CheckForDroppedObjects()
	{
		object[] droppedObjects = ObjectPainterEditor.DropZone(m_objectPropertiesWindow);
		if (droppedObjects != null)
		{
			int objectCount = droppedObjects.Length;
			for (int i = 0; i < objectCount; ++i)
			{
				GameObject currentObject = (GameObject)droppedObjects[i];
				s_objectPainterInstance.CreatePainterObject(currentObject, false);
			}

		}
	}

	/// <summary>
	/// This method displays the whole object list rom the PaintManager class.
	/// Each individual PaintObject from the PaintManager is displayed and has its own
	/// set of properties which the user can change and alter.
	/// </summary>
	private void DisplayPaintObjectProperties()
	{
		GUILayout.BeginArea(m_objectPropertiesWindow, GUI.skin.box);

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Add object"))
			s_objectPainterInstance.AddPainterObject(new PaintObject());

		GUILayout.EndHorizontal();
		EditorGUILayout.Space();

		GUILayout.BeginHorizontal();
		GUILayout.Label("\nActive", GUILayout.MaxWidth(40.0f));
		GUILayout.Label("Keyboard\nShortcut", GUILayout.MaxWidth(70.0f));
		GUILayout.Label("\nPaint Object", GUILayout.MaxWidth(80.0f));

		GUILayout.EndHorizontal();

		m_objectListScrollPos = EditorGUILayout.BeginScrollView(m_objectListScrollPos);

		DisplayObjectProperties((m_objectPropertiesWindow.height / 4.0f));

		EditorGUILayout.EndScrollView();
		GUILayout.EndArea();
	}

	/// <summary>
	/// Displays all different control settings the user can make.
	/// </summary>
	private void DisplayKeyboardProperties()
	{
		GUI.skin.label.alignment = TextAnchor.MiddleLeft;
		GUILayout.BeginArea(new Rect(350.0f, 10.0f, 200.0f, 250.0f), GUI.skin.box);
		EditorGUILayout.LabelField("Controls:", EditorStyles.boldLabel);
		EditorGUILayout.Space();

		DisplayInputSettingProperty("Add and snap object to target:", " + ", s_keyboardSnapToObject, new KeyboardShortcutKeys[] { s_keyboardAddObjectToScene }, s_mouseSnapToObject);
		DisplayInputSettingProperty("Add object to scene:", " + ", s_keyboardAddObjectToScene, new KeyboardShortcutKeys[] { s_keyboardSnapToObject }, s_mouseAddObjectToScene);
		DisplayPlaceEmptyObjectProperties();
		GUILayout.EndArea();
	}

	/// <summary>
	/// Displays property settings for placing a object into empty space, meaning the user has not clicked
	/// on any object when activating the Paint object method. These settings will make it easier and better 
	/// to place a object into empty space.
	/// </summary>
	private void DisplayPlaceEmptyObjectProperties()
	{
		EditorGUILayout.LabelField("Paint object without target:");
		EditorGUILayout.BeginVertical(GUI.skin.box);
		s_emptyObjectPlacementPropertyIndex = EditorGUILayout.Popup(s_emptyObjectPlacementPropertyIndex, m_emptyObjectPlacementProperty);
		s_emptyObjectPlacementPropertyVector = EditorGUILayout.Vector3Field(string.Empty, s_emptyObjectPlacementPropertyVector);
		EditorGUILayout.EndVertical();
	}

	/// <summary>
	/// A GUI method for keyboard and mouse settings. Normally each keyboard/mouse setting must have its own individual settings. No keyboard settings are allowed to have the same setup.
	/// So this method displays the available settings and makes a check so that the new settings aren't already in use. If the settings are already in use, then
	/// the settings are reverted back to its previous state.
	/// </summary>
	/// <param name="keyboardInfoText">The keyboard information text for the GUI obejct.</param>
	/// <param name="mouseInfoText">The mouse information text for the GUI object.</param>
	/// <param name="keyboardShortcut">The current keyboard shortcut which is linked to the current settings property.</param>
	/// <param name="checkAgainstKeyboardShortcut">A array of already existing keyboard shortcuts, which the new shortcut is not allowed to have.</param>
	/// <param name="mouseInput">The current type of mouse input which is linked to the current settings property.</param>
	private void DisplayInputSettingProperty(string keyboardInfoText, string mouseInfoText, KeyboardShortcutKeys keyboardShortcut, KeyboardShortcutKeys[] checkAgainstKeyboardShortcut, MouseInputType mouseInput)
	{
		EditorGUILayout.LabelField(keyboardInfoText);
		EditorGUILayout.BeginHorizontal(GUI.skin.box);
		KeyboardShortcutKeys tempValue = (KeyboardShortcutKeys)EditorGUILayout.EnumPopup(keyboardShortcut, GUILayout.MinWidth(80.0f));

		if (tempValue == KeyboardShortcutKeys.None)
			keyboardShortcut = tempValue;

		int objectCount = checkAgainstKeyboardShortcut.Length;
		bool foundMatch = false;
		for (int i = 0; i < objectCount; ++i)
		{
			if (tempValue == checkAgainstKeyboardShortcut[i])
			{
				foundMatch = true;
				break;
			}

		}

		if (foundMatch == false)
			keyboardShortcut = tempValue;

		EditorGUILayout.LabelField(mouseInfoText, GUILayout.MaxWidth(20.0f));
		s_mouseSnapToObject = (MouseInputType)EditorGUILayout.EnumPopup(mouseInput);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();
	}

	/// <summary>
	/// Displays the whole PaintObject list from the PaintManager class.
	/// The player can change the settings for each individual object which appears in the list.
	/// If there is no available objects in the list, a text is displayed with a given space size from the top.
	/// </summary>
	/// <param name="spaceSize">The space size used when there is no objects in the PaintObject list.</param>
	private void DisplayObjectProperties(float spaceSize)
	{
		int objectCount = s_objectPainterInstance.ObjectCount;

		if (objectCount == 0)
		{
			TextAnchor oldAlignment = GUI.skin.label.alignment;
			GUI.skin.label.alignment = TextAnchor.MiddleCenter;
			GUILayout.Space(spaceSize);
			EditorGUILayout.LabelField("No objects found. Press the 'Add object'", GUI.skin.label);
			EditorGUILayout.LabelField("button to add objects to the list.", GUI.skin.label);
			EditorGUILayout.LabelField("Or drag and drop prefab object(s)", GUI.skin.label);
			EditorGUILayout.LabelField("into the list field.", GUI.skin.label);
			GUI.skin.label.alignment = oldAlignment;
			return;
		}

		for (int index = 0; index < objectCount; ++index)
		{
			PaintObject currentObject = s_objectPainterInstance.GetPainterObject(index);
			GUILayout.BeginHorizontal("Box");
			bool activate = GUILayout.Toggle(currentObject.IsActive, string.Empty, new GUILayoutOption[] { GUILayout.MaxWidth(30.0f), GUILayout.MinWidth(30.0f) });

			if (activate == true)
			{
				s_objectPainterInstance.SetActivePainterObject(index);
			}
			else
				currentObject.IsActive = false;

			int newIndexValue = EditorGUILayout.Popup(currentObject.ShortCutIndex, m_shortcutValue, new GUILayoutOption[] { GUILayout.MaxWidth(80.0f), GUILayout.MinWidth(40.0f) });
			s_objectPainterInstance.SetShortcutIndex(index, newIndexValue);
			GameObject tempObject = (GameObject)EditorGUILayout.ObjectField(currentObject.PaintGameObject, typeof(GameObject), true);

			if (tempObject != currentObject.PaintGameObject)
				s_objectPainterInstance.AddGameObjectToList(currentObject, tempObject, false);

			if (GUILayout.Button("Remove") == true)
			{
				s_objectPainterInstance.RemovePainterObject(index);
				GUILayout.EndHorizontal();
				return;
			}
			else
				GUILayout.EndHorizontal();
		}
	}
}
