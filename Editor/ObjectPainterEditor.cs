/*************************************************
The MIT License (MIT)
	
Copyright (c) 2014 Mikael "DevMikaelNilsson" Nilsson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*************************************************/
using UnityEngine;
using UnityEditor;
using System.Collections;
using ObjectPainter;

/// <summary>
/// A input editor window for the ObjectPainter window.
/// This script handles most inputs and derive directly from the ObjectPainterWindow.
/// </summary>
[InitializeOnLoad]
public class ObjectPainterEditor : ObjectPainterWindow
{
	private static ChosenPaintObjectProperties s_lastChosenObjectProperties = null;

	/// <summary>
	/// This method creates a new object from a target object and places it on the screen point which 
	/// where the user clicked. The method also makes sure, when possible, that the new created object is not
	/// placed inside the target obejct, but is placed ontop of the object. This can only be done if there is a valid collider
	/// on the target object.
	/// </summary>
	/// <param name="targetObject">The target object which the used has clicked on in the scene view window. This value can not be null.</param>
	/// <param name="activeObject">The current active PaintObject which is activated through the ObjectPainterWindow script.</param>
	/// <param name="point">The actual point in worldspace where the user has clicked.</param>
	private static void HandleObjectPaintInput(Transform targetObject, PaintObject activeObject, Vector3 point)
	{
		float yOffsetvalue = 0.0f;

		if (targetObject != null)
		{
			if (targetObject.collider != null)
				yOffsetvalue = targetObject.collider.bounds.max.y;
			else if (targetObject.collider2D != null)
			{
				Vector3 size = Get2dColliderSize(targetObject);
				yOffsetvalue = size.y;
			}
		}

		GameObject newCreatedGameObject = (GameObject)GameObject.Instantiate(activeObject.PaintGameObject, point, Quaternion.identity);

		if (newCreatedGameObject.collider != null)
			point.y = yOffsetvalue + (newCreatedGameObject.collider.bounds.size.y / 2.0f);
		if (newCreatedGameObject.collider2D != null)
		{
			Vector3 size = Get2dColliderSize(newCreatedGameObject.transform);
			point.y = yOffsetvalue + (size.y);
		}

		newCreatedGameObject.name = activeObject.PaintGameObject.name;
		newCreatedGameObject.transform.position = point;
		Selection.activeGameObject = newCreatedGameObject;
	}

	/// <summary>
	/// This method creates a new object from a target object, and snaps it to the given side of the object.
	/// Where the object is placed is based on the face of the object, where the user clicks on and what the normal value this face has when the user clicks on it.
	/// This can only be done properly if there is a valid collider on the target object.
	/// </summary>
	/// <param name="targetObject">The target object which the used has clicked on in the scene view window. This value can not be null.</param>
	/// <param name="activeObject">The current active PaintObject which is activated through the ObjectPainterWindow script.</param>
	/// <param name="point">The actual point in worldspace where the user has clicked.</param>
	/// <param name="normalPoint">The normal value for the target object which the user has clicked on.</param>
	private static void HandleObjectPaintInput(Transform targetObject, PaintObject activeObject, Vector3 point, Vector3 normalPoint)
	{
		Vector3 finalPosition = Vector3.zero;
		Vector3 newCreatedObjectSize = Vector3.zero;
		Vector3 targetBoundsSize = Vector3.zero;
		Quaternion localRotation = Quaternion.identity;

		if (targetObject != null)
		{
			finalPosition = targetObject.localPosition;
			localRotation = targetObject.localRotation;

			if (targetObject.collider != null)
				targetBoundsSize = (targetObject.collider.bounds.size / 2.0f);
			else if (targetObject.collider2D != null)
			{
				targetBoundsSize = Get2dColliderSize(targetObject);
				normalPoint = CalculateSpriteEdge(targetObject, point);
			}
			else
				Debug.LogWarning("Collider object is missing.");


		}
		else
		{
			finalPosition = point;
		}

		GameObject newCreatedGameObject = (GameObject)GameObject.Instantiate(activeObject.PaintGameObject, finalPosition, localRotation);


		if (newCreatedGameObject.collider != null)
			newCreatedObjectSize = (newCreatedGameObject.collider.bounds.size / 2.0f);
		else if (newCreatedGameObject.collider2D != null)
			newCreatedObjectSize = Get2dColliderSize(newCreatedGameObject.transform);
		else
		{
			Debug.LogWarning("No collider object was found on object: " + newCreatedGameObject);
			newCreatedObjectSize = Vector3.one;
		}

		finalPosition.x += ((targetBoundsSize.x + newCreatedObjectSize.x) * normalPoint.x);
		finalPosition.y += ((targetBoundsSize.y + newCreatedObjectSize.y) * normalPoint.y);
		finalPosition.z += ((targetBoundsSize.z + newCreatedObjectSize.z) * normalPoint.z);
		newCreatedGameObject.name = activeObject.PaintGameObject.name;
		newCreatedGameObject.transform.localPosition = finalPosition;
		Selection.activeGameObject = newCreatedGameObject;
	}

	/// <summary>
	/// Calculates the possible size for a sprite object. The size of the sprite object can 
	/// only be calculated if the sprite object has a 2d collider which is supported. If the sprite
	/// doesn't have a 2d collider, or using a unsupported collider, then the method returns a value of zero.
	/// </summary>
	/// <param name="target2dObject">The sprite object which size will be calculated.</param>
	/// <returns>The size of the sprite object. If the sprite object doesn't have a collider, or if the collider is not supported, 
	/// then a default value (Vector2.zero) is returned.</returns>
	private static Vector3 Get2dColliderSize(Transform target2dObject)
	{
		BoxCollider2D boxColliderObject = target2dObject.GetComponent<BoxCollider2D>();
		CircleCollider2D circleColliderObject = target2dObject.GetComponent<CircleCollider2D>();
		Vector3 sizeVector = Vector3.zero;
		if (boxColliderObject != null)
		{
			sizeVector = (new Vector3(boxColliderObject.size.x, boxColliderObject.size.y, 0.0f) / 2.0f);
		}
		else if (circleColliderObject != null)
			sizeVector = (new Vector3(circleColliderObject.radius, circleColliderObject.radius, 0.0f) / 2.0f);
		else
			Debug.LogWarning("2D collider is currently not supported.");

		return sizeVector;
	}

	/// <summary>
	/// Calculates which edge of the sprite is the closest to the input point position.
	/// Currently this only chooses either top/bottom or left/right edge, and doesn't return 
	/// a diagonal edge value.
	/// </summary>
	/// <param name="target2dObject">The sprite object which the user has clicked on.</param>
	/// <param name="point">The mouse point position in the world space.</param>
	/// <returns>The closest edge as a normal-based point value.</returns>
	private static Vector3 CalculateSpriteEdge(Transform target2dObject, Vector3 point)
	{
		Vector3 edgeValue = Vector3.zero;
		Vector3 size = Get2dColliderSize(target2dObject);

		float leftXvalue = Mathf.Abs((target2dObject.localPosition.x - size.x) - point.x);
		float rightXvalue = Mathf.Abs((target2dObject.localPosition.x + size.x) - point.x);
		float leftYvalue = Mathf.Abs((target2dObject.localPosition.y - size.y) - point.y);
		float rightYvalue = Mathf.Abs((target2dObject.localPosition.y + size.y) - point.y);

		float bestX = leftXvalue;
		float bestY = leftYvalue;

		if (rightXvalue < bestX)
			bestX = rightXvalue;
		if (rightYvalue < bestY)
			bestY = rightYvalue;

		if (bestX < bestY)
		{
			edgeValue.x = -1.0f;
			if (bestX == rightXvalue)
				edgeValue.x = 1.0f;
		}
		else
		{
			edgeValue.y = -1.0f;
			if (bestY == rightYvalue)
				edgeValue.y = 1.0f;
		}

		return edgeValue;
	}

	/// <summary>
	/// This method is called through a delegate call in the ObjectPainterWindow. The delegate 
	/// is automatically called from the Unity editor.
	/// The method checks for a valid input and if the input is fired, then the proper methods are called.
	/// </summary>
	/// <param name="sceneView">The current scene view object.</param>
	public static void UpdateView(SceneView sceneView)
	{
		CheckForSpecialInput();		
		if (Event.current.type == EventType.MouseDown)
		{
			if (s_lastChosenObjectProperties != null && s_lastChosenObjectProperties.CurrentPaintObject != null)
				FireRayCast();
		}
	}

	/// <summary>
	/// Activates  a raycast to find a target object. A target object is a object which the user aims the mouse pointer
	/// at when activating a ObjectPainter method. The method tries several ways to find a taget object, if any step
	/// successfully finds a target object, then the rest of the steps are ignored:
	/// 1. The method activates a 3D raycast to find a target object.
	/// 2. The method activates a 2D raycast to find a 2D (sprite) object.
	/// 3. The method activates the HandleUtility.PickGameObject method, which searches wider for a picked game object.
	/// 4. The method calculates a world position for the new object to be placed in the empty world space based on the
	/// settings from the ObjectPainter editor window.
	/// </summary>
	private static void FireRayCast()
	{
		float raycastDistance = 1000.0f;
		float posy = (Camera.current.pixelHeight - Event.current.mousePosition.y);
		Vector3 recalculatedInputPosition = new Vector3(Event.current.mousePosition.x, posy, 0.0f);
		Ray ray = Camera.current.ScreenPointToRay(recalculatedInputPosition);
		RaycastHit hitInfo = new RaycastHit();

		if (Physics.Raycast(ray, out hitInfo, raycastDistance) == true)
			HandleHitInput(hitInfo.transform, hitInfo.point, hitInfo.normal);
		else
		{
			RaycastHit2D rayCastCollider2d = Physics2D.Raycast(ray.origin, ray.direction, raycastDistance);
			if (rayCastCollider2d.transform != null)
				HandleHitInput(rayCastCollider2d.transform, rayCastCollider2d.point, rayCastCollider2d.normal);
			else
			{
				GameObject possibleGameObject = HandleUtility.PickGameObject(Event.current.mousePosition, true);
				if (possibleGameObject != null)
					HandleHitInput(possibleGameObject.transform, ray.origin, Vector3.zero);
				else
					PlaceObjectWithoutTargetObject(ray.origin, ray.direction);
			}
		}
	}

	/// <summary>
	/// If the user places a object, without a target obejct (directly clicking on a object), then the method will
	/// calculate the proper place to position the object based on the settings from the editor window.
	/// </summary>
	/// <param name="origin">The origin position in the scene view. This should be the position where the mouse input was detected.</param>
	/// <param name="direction">The camera direction, or the direction for the mouse input.</param>
	private static void PlaceObjectWithoutTargetObject(Vector3 origin, Vector3 direction)
	{
		Vector3 finalPosition = Vector3.zero;
		switch (s_emptyObjectPlacementPropertyIndex)
		{
		case 0:
			finalPosition.x = (origin.x + (direction.x * s_emptyObjectPlacementPropertyVector.x));
			finalPosition.y = (origin.y + (direction.y * s_emptyObjectPlacementPropertyVector.y));
			finalPosition.z = (origin.z + (direction.z * s_emptyObjectPlacementPropertyVector.z));
			break;
		case 1:
			finalPosition = s_emptyObjectPlacementPropertyVector;
			break;
		}

		HandleHitInput(null, finalPosition, Vector3.zero);
	}

	/// <summary>
	/// If the right input is fired, and the user has clicked on a object, then this method checks to see if the right combination
	/// of keyboard inputs are used in order to fire a method.
	/// </summary>
	/// <param name="hitTransform">The transform component of a object which the player has clicked on.</param>
	/// <param name="hitPoint">The world point where the user has clicked.</param>
	/// <param name="hitNormal">The normal value for the object the user has clicked on.</param>
	private static void HandleHitInput(Transform hitTransform, Vector3 hitPoint, Vector3 hitNormal)
	{
		if (CheckObjectInput(s_keyboardAddObjectToScene, s_mouseAddObjectToScene) == true)
		{
			HandleObjectPaintInput(hitTransform, s_lastChosenObjectProperties.CurrentPaintObject, hitPoint);
			ResetInputProperties();
		}
		else if (CheckObjectInput(s_keyboardSnapToObject, s_mouseSnapToObject) == true)
		{
			HandleObjectPaintInput(hitTransform, s_lastChosenObjectProperties.CurrentPaintObject, hitPoint, hitNormal);
			ResetInputProperties();
		}
	}

	/// <summary>
	/// Resets the stored input properties and eats up the input so that the input is not passed on to other components.
	/// </summary>
	private static void ResetInputProperties()
	{
		s_lastChosenObjectProperties.Reset();
		Event.current.Use();
	}

	/// <summary>
	/// Method makes a check to see if a current set of shortcuts has been activated by the event input.
	/// </summary>
	/// <param name="currentKeyboardSetting">The current keyboard input settings to check.</param>
	/// <param name="currentMouseInputSetting">The current mouse input settings to cehck.</param>
	/// <returns>Returns true if the keyboard/mouse combination has a match. Return false otherwise.</returns>
	private static bool CheckObjectInput(KeyboardShortcutKeys currentKeyboardSetting, MouseInputType currentMouseInputSetting)
	{
		int KeyboardIndexValue = (int)currentKeyboardSetting;
		int mouseIndexValue = (int)currentMouseInputSetting;

		if (Event.current.button == mouseIndexValue)
		{
			if (KeyboardIndexValue == 0 && s_lastChosenObjectProperties.CtrlIsActive == false && s_lastChosenObjectProperties.ShiftIsActive == false)
				return true;
			else if (KeyboardIndexValue == 1 && s_lastChosenObjectProperties.CtrlIsActive == true)
				return true;
			else if (KeyboardIndexValue == 2 && s_lastChosenObjectProperties.ShiftIsActive == true)
				return true;
		}

		return false;
	}

	/// <summary>
	/// This method checks for if a user clicks on a object shortcut or using a special key (ctrl, shift etc).
	/// If the user has clicked on a shortcut then the proper object will be activated.
	/// The object and specific key inputs are stored in a properties object which can be later used.
	/// </summary>
	private static void CheckForSpecialInput()
	{
		if (s_objectPainterInstance == null)
			return;

		if (s_lastChosenObjectProperties == null)
			s_lastChosenObjectProperties = new ChosenPaintObjectProperties();

		int index = -1;
		switch (Event.current.keyCode)
		{
		case KeyCode.F1:
			index = s_objectPainterInstance.GetShortcutObjectIndex(1);
			break;
		case KeyCode.F2:
			index = s_objectPainterInstance.GetShortcutObjectIndex(2);
			break;
		case KeyCode.F3:
			index = s_objectPainterInstance.GetShortcutObjectIndex(3);
			break;
		case KeyCode.F4:
			index = s_objectPainterInstance.GetShortcutObjectIndex(4);
			break;
		case KeyCode.F5:
			index = s_objectPainterInstance.GetShortcutObjectIndex(5);
			break;
		case KeyCode.F6:
			index = s_objectPainterInstance.GetShortcutObjectIndex(6);
			break;
		case KeyCode.F7:
			index = s_objectPainterInstance.GetShortcutObjectIndex(7);
			break;
		case KeyCode.F8:
			index = s_objectPainterInstance.GetShortcutObjectIndex(8);
			break;
		case KeyCode.F9:
			index = s_objectPainterInstance.GetShortcutObjectIndex(9);
			break;
		case KeyCode.F10:
			index = s_objectPainterInstance.GetShortcutObjectIndex(10);
			break;
		case KeyCode.F11:
			index = s_objectPainterInstance.GetShortcutObjectIndex(11);
			break;
		case KeyCode.F12:
			index = s_objectPainterInstance.GetShortcutObjectIndex(12);
			break;
		}

		if (index != -1)
			s_objectPainterInstance.SetActivePainterObject(index);

		s_lastChosenObjectProperties.CurrentPaintObject = s_objectPainterInstance.GetActivePainterObject();
		s_lastChosenObjectProperties.CtrlIsActive = Event.current.control;
		s_lastChosenObjectProperties.ShiftIsActive = Event.current.shift;
	}

	/// <summary>
	/// Method to check if there is a drag and drop input in a given rect area.
	/// The player may drag prefab objects and drop them in the given zone. If there is such a 
	/// drop input then this method returns a array of objects which was dropped in this zone.
	/// </summary>
	/// <param name="dropZoneRect">The rect size of the area which a drag and drop input is allowed.</param>
	/// <returns>A array of objects which were dropped in the given zone. Otherwise the array is null.</returns>
	public static object[] DropZone(Rect dropZoneRect)
	{
		GUILayout.BeginArea(dropZoneRect);

		EventType eventType = Event.current.type;
		bool isAccepted = false;

		if (eventType == EventType.DragUpdated || eventType == EventType.DragPerform)
		{
			DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

			if (eventType == EventType.DragPerform)
			{
				DragAndDrop.AcceptDrag();
				isAccepted = true;
			}
			Event.current.Use();
		}

		GUILayout.EndArea();

		if (isAccepted == true)
			return DragAndDrop.objectReferences;
		else
			return null;
	}
}